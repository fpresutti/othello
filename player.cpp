#include "player.h"

Player::Player(Side side) {
    cerr << "Initializing player" << endl;
    us = side;
    them = (side == BLACK ? WHITE : BLACK);
    if (us == BLACK) turn = 0; // black always starts
    else turn = 1;
    gamestage = 1;
    board = new Board();
    // Initialize GameState tree
    Move *dummymove = new Move(0, 0);
    currstate = new GameState(board, dummymove,
        BLACK, (us == BLACK ? BLACK : WHITE));
    delete dummymove;
    // branchout two? levels
    cerr << "Branching out game beginning" << endl;
    currstate->branchout();
    for (unsigned int a = 0; a < currstate->nextmoves.size(); a++) {
        GameState* p = currstate->nextmoves[a];
        p->branchout();
        for (unsigned int b = 0; b < p->nextmoves.size(); b++) {
            p->nextmoves[b]->branchout();
        }
    }
    cerr << "Done initializing" << endl;
}

Player::~Player() {
    delete currstate;
}

Move *Player::doMove(Move *opponentsMove, int msLeft) {
    cerr << "New turn" << endl;
    // Process opponent move
    int x, y;
    if (opponentsMove) {
        turn += 2; // one for you one for me
        x = opponentsMove->getX();
        y = opponentsMove->getY();
        cerr << "Message for good measure" << endl;
        cerr << "Their move: " << opponentsMove << ", " << opponentsMove->getX() << ", " << opponentsMove->getY() << endl;
    }
    else { // NULL move
        x = y = 3;
        turn += 1; // one for me
        cerr << "The sissies passed their move" << endl;
    }
    board->doMove(opponentsMove, them);
    
    // Go down GameState tree, deleting all but branch we went down.
    bool movenotfound = true;
    for (unsigned int a = 0; a < currstate->nextmoves.size(); a++) {
        cerr << currstate->nextmoves[a]->moveX() << ", " << currstate->nextmoves[a]->moveY() << endl;
        if (currstate->nextmoves[a]->moveX() == x
         && currstate->nextmoves[a]->moveY() == y) {
            GameState *temp = currstate;
            currstate = currstate->nextmoves[a];
            temp->nextmoves.erase(temp->nextmoves.begin() + a);
            delete temp;
            movenotfound = false;
            break;
            }
        }
    if (movenotfound) { // For debugging purposes
            cerr << "Error: opponent move not in tree" << endl;
        } else cerr << "Processed opponent move" << endl;

    // Now regrow GameState tree, and determine best move with minimax
    // try to maximize our score
    // try to minimize their score
    GameState* bestnextmove = NULL;
    int bestnextindx = 0;
    int maxscore1 = -BIGNUM;
    vector<GameState*> *p1 = &(currstate->nextmoves);
    currstate->branchout();//?
    for (unsigned int a1 = 0; a1 < p1->size(); a1++) {               // looking at our next move
        p1->at(a1)->branchout();//?
        vector<GameState*> *p2 = &(p1->at(a1)->nextmoves);
        int minscore1 = BIGNUM;

        for (unsigned int b1 = 0; b1 < p2->size(); b1++) {           // looking at their response
            p2->at(b1)->branchout();
            vector<GameState*> *p3 = &(p2->at(b1)->nextmoves);
            int maxscore2 = -BIGNUM;

            for (unsigned int a2 = 0; a2 < p3->size(); a2++) {       // looking at our move after
                p3->at(a2)->branchout();
                int minscore2 = BIGNUM;
                vector<GameState*> *p4 = &(p3->at(a2)->nextmoves);

                for (unsigned int b2 = 0; b2 < p4->size(); b2++) {   // finally look at their response
                    p4->at(b2)->branchout();
                    int maxscore3 = -BIGNUM;
                    
                    if (p4->at(b2)->nextmoves.size())                // look at resulting score
                    for (unsigned int a3 = 0; a3 < p4->at(b2)->nextmoves.size(); a3++) {
                        if (maxscore3 < p4->at(b2)->nextmoves[a3]->score()) {
                            maxscore3 = p4->at(b2)->nextmoves[a3]->score();
                        }
                    }

                    if (maxscore3 < minscore2) {
                        minscore2 = maxscore3;
                    }
                    if (maxscore3 >= maxscore2) {
                        break;
                    }
                }

                if (minscore2 > maxscore2) {
                    maxscore2 = minscore2;
                }
                if (minscore2 <= minscore1) {
                    break;
                }

            }

            if (maxscore2 < minscore1) {
                minscore1 = maxscore2;
            }
            if (maxscore2 >= maxscore1) {
                break;
            }

        }
        if (minscore1 > maxscore1) {
            cerr << "Found new best move " << a1 << endl;
            maxscore1 = minscore1;
            bestnextmove = p1->at(a1);
            bestnextindx = a1;
        }
    }
    
    Move *ourmove = NULL;
    if (bestnextmove) {
        ourmove = new Move(bestnextmove->moveX(), bestnextmove->moveY());
        board->doMove(ourmove, us);
        GameState *temp = currstate;
        currstate = bestnextmove;
        temp->nextmoves.erase(temp->nextmoves.begin() + bestnextindx);
        delete temp;
        cerr << "Picked move: " << ourmove->getX() << ", " << ourmove->getY() << endl;
    }
    return ourmove;
}
