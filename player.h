#include "GameState.h"

#define BIGNUM 1000


class Player {
private:
    Side us;
    Side them;
    Board *board;
    unsigned int turn;
    int gamestage;
    GameState *currstate;

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    bool testingMinimax;
    
};
