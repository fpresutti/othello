Improvements:

Went from a messy recursion based gameplay to a gametree based gamplay. I thought this would be the most hassle-free way of doing things. Although not completely optimized for memory use, I am satisfied by how it works for now. 
The player class has a gamestate class pointer, the root of the tree. Every time the opponent plays a move, we go down the tree and regrow it. We use the scores at the bottom of the tree to implement our move heuristic.
Also alpha-beta pruning was implemented for every depth-level.
The heuristics are similar to last time, but improved slightly.

What I was thinking of doing was have different heuristics based on the stage of the game that was being played. Early game one should focus on clumping their stones and not opening up new frontiers. Middle game, when the edges have been opened up, it is good to try creeping up the edges. At the end of the game one should focus on simply making sure to get quantity of stones. Unfortunately I have other things to do.
