#ifndef GAMESTATE
#define GAMESTATE

#include "common.h"
#include "board.h"
#include <vector>
#include <iostream>
using namespace std;


class GameState {
private:
    Board *currboard;               // board state
    Move togethere;                 // move implemented to get to this move
    Side side;                      // which side is playing this move
    Side us;                        // who is trying to win this game
    int scr;                        // our heuristic numerical score for this state
    bool branched;                  // whether we have branched out from this state yet
    int calcscore(int i, int j);
public:
    GameState(Board* oldboard, Move *gethere, Side playerturn, Side player);
    ~GameState();
    
    int score() { return scr; };
    void branchout();
    int moveX() { return togethere.getX(); };
    int moveY() { return togethere.getY(); };
    vector<GameState*> nextmoves;   // vector that branches out to all next moves
};

#endif