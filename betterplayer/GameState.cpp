#include "GameState.h"

GameState::GameState(Board* oldboard, Move *gethere, Side playerturn, Side player) : togethere(gethere->getX(), gethere->getY()) {
    side = playerturn;
    currboard = oldboard->copy();
    currboard->doMove(&togethere, side);
    us = player;
    scr = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            scr += currboard->getpos(us, i, j) * calcscore(i, j);
        }
    }
}

GameState::~GameState() {
    delete currboard; // and delete all children in tree
    if (nextmoves.size()) {
        for (unsigned int a = 0; a < nextmoves.size(); a++) {
            delete nextmoves[a];
        }
    }
}

int GameState::calcscore(int i, int j) {
    if (i % 7 == j % 7 && i % 7 == 0) {
        return 15; // Corner spot
    }
    else if (i % 5 == 1  && j % 5 == 1) {
        return 1; // X cell
    }
    else if ((i % 5 == 1 || i % 7 == 0) && (j % 5 == 1 || j % 7 == 0)) {
        return 2; // C cell
    }
    else if (i % 7 == 0 || j % 7 == 0) {
        return 4; // Third frontier
    }
    else if (i % 5 == 1 || j % 5 == 1) {
        return 3; // Second frontier
    }
    else if (i % 3 == 2 || j % 3 == 1) {
        return 2; // First frontier
    }
    else return 1;
}

void GameState::branchout() {
    if (branched) return;
    // Find and record all our possible moves, pruning results
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (currboard->checkMove(&move, us)) {
                GameState *nextmove = new GameState(currboard, &move,\
                    (side == BLACK ? WHITE : BLACK), (us == BLACK ? WHITE : BLACK));
                nextmoves.push_back(nextmove);
            }
        }
    }
    branched = true;
}
