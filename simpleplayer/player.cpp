#include "player.h"


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    us = side;
    them = (side == BLACK ? WHITE : BLACK);
    if (us == BLACK) turn = 0; // black always starts
    else turn = 1;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    // Process opponent move
    if (opponentsMove) {
        board.doMove(opponentsMove, them);
        turn += 2; // one for you one for me
    }
    else turn += 1; // one for me
    
    // Find and record all moves
    vector<Coord> moves;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (board.checkMove(&move, us)) {
                moves.push_back(Coord(i, j));
            }
        }
    }
    if (moves.size() == 0) return NULL;
    
    unsigned int bestmove = 0; // index for best move in vector
    int bestscore = score(moves[0]); // score for best move in vector
    for (unsigned int i = 1; i < moves.size(); i++) {
        int scr = score(moves[i]);
        if (scr > bestscore) {
            bestscore = scr;
            bestmove = i;
        }
    }
    
    Move *ourmove = new Move(moves[bestmove].i, moves[bestmove].j);
    board.doMove(ourmove, us);
    return ourmove;
}


int Player::score(Coord &c) {
    if (c.i % 7 == c.j % 7 && c.i % 7 == 0) {
        return 5; // Corner spot
    }
    else if (c.i % 5 == 1  && c.j % 5 == 1) {
        return 0; // X cell
    }
    else if ((c.i % 5 == 1 || c.i % 7 == 0) && (c.j % 5 == 1 || c.j % 7 == 0)) {
        return 1; // C cell
    }
    else if (c.i % 7 == 0 || c.j % 7 == 0) {
        return 3; // Third frontier
    }
    else if (c.i % 5 == 1 || c.j % 5 == 1) {
        return 2; // Second frontier
    }
    else if (c.i % 3 == 2 || c.j % 3 == 1) {
        return 4; // First frontier
    }
    else return 2;
}
