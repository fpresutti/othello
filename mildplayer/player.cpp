#include "player.h"


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    us = side;
    them = (side == BLACK ? WHITE : BLACK);
    if (us == BLACK) turn = 0; // black always starts
    else turn = 1;
    gamestage = 1;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    // Process opponent move
    if (opponentsMove) {
        board.doMove(opponentsMove, them);
        turn += 2; // one for you one for me
    }
    else turn += 1; // one for me
    
    // Find and record all moves
    vector<Coord> moves;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (board.checkMove(&move, us)) {
                moves.push_back(Coord(i, j));
            }
        }
    }
    if (moves.size() == 0) return NULL;
    
    unsigned int bestmove = 0; // index for best move in vector
    int bestscore = minimax(board, moves[0], DEPTH); // score for best move in vector
    cerr << "Score for " << moves[0].i << moves[0].j << " is " << bestscore << endl;
    //
    for (unsigned int i = 1; i < moves.size(); i++) {
        int scr = minimax(board, moves[i], DEPTH);
        if (scr > bestscore) {
            bestscore = scr;
            bestmove = i;
        }
        cerr << "Score for " << moves[i].i << moves[i].j << " is " << scr << endl;
        //
    }
    
    Move *ourmove = new Move(moves[bestmove].i, moves[bestmove].j);
    board.doMove(ourmove, us);
    return ourmove;
}

int Player::minimax(Board &origbrd, Coord &c, int depth) {
    // Copy board and check find every opponent move, branching out new boards
    Board brdcpy = *origbrd.copy();
    Move origmove(c.i, c.j);
    brdcpy.doMove(&origmove, us);
    vector<Board*> branches;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (brdcpy.checkMove(&move, them)) {
                Board *brd = brdcpy.copy();
                brd->doMove(&move, them);
                branches.push_back(brd);
            }
        }
    }
    int worstscore = BIGNUM;
    // If we want to sprout deeper, continue until depth == 0
    if (depth) {
        // for all the found boards go deeper
        for (unsigned int a = 0; a < branches.size(); a++) {
            // Find and record all moves
            vector<Coord> moves;
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    Move move(i, j);
                    if (branches[a]->checkMove(&move, us)) {
                        moves.push_back(Coord(i, j));
                    }
                }
            }
            // for all moves, get a score
            for (unsigned int i = 0; i < moves.size(); i++) {
                int currscore = minimax(*branches[a], moves[i], depth - 1);
                if (currscore < worstscore) {
                    worstscore = currscore;
                }
            }
        }
    }
    // Calculate score (base case)
    else {
        for (unsigned int a = 1; a < branches.size(); a++) {
            int currscore = 0;
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    currscore += branches[a]->getpos(us, i, j) * score(i, j);
                }
            }
            if (currscore < worstscore) {
                worstscore = currscore;
            }
        }
    }
    return worstscore;
}

int Player::score(int i, int j) {
    if (i % 7 == j % 7 && i % 7 == 0) {
        return 15; // Corner spot
    }
    else if (i % 5 == 1  && j % 5 == 1) {
        return 1; // X cell
    }
    else if ((i % 5 == 1 || i % 7 == 0) && (j % 5 == 1 || j % 7 == 0)) {
        return 2; // C cell
    }
    else if (i % 7 == 0 || j % 7 == 0) {
        return 4; // Third frontier
    }
    else if (i % 5 == 1 || j % 5 == 1) {
        return 3; // Second frontier
    }
    else if (i % 3 == 2 || j % 3 == 1) {
        return 2; // First frontier
    }
    else return 1;
}
