#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

#define DEPTH 0
#define BIGNUM 1000

struct Coord {
    int i;
    int j;
    Coord(int i, int j) {
        this->i = i;
        this->j = j;
    }
};


class Player {
private:
    Side us;
    Side them;
    Board board;
    unsigned int turn;
    int score(int i, int j);
    int gamestage;
    int minimax(Board &origbrd, Coord &c, int depth);

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
